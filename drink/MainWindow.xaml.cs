﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using weka.classifiers;
using weka.core;
using weka.filters;

namespace drink
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        Classifier classifier;

        Instances instances120 = new Instances(new java.io.FileReader("drink(120).arff"));
        Instances instances140 = new Instances(new java.io.FileReader("drink(140).arff"));
        Instances instances200 = new Instances(new java.io.FileReader("drink(200).arff"));

        public List<AgeItems> age { get; set; }
        public List<GenderItems> gender { get; set; }
        public List<CaffeineItems> caffeine { get; set; }
        public List<MilkItems> milk { get; set; }
        public List<SweetnessItems> sweetness { get; set; }
        public List<PriceItems> price { get; set; }
        public List<DrinkItems> drink { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            value_setting();

            oneR.IsChecked = true;
        }

        private void value_setting()
        {
            classValue.Text = "";
            accuracy.Text = "";
            result.Text = "";

            age = new List<AgeItems>()
            {
                new AgeItems() { name = "< 선택 >", value = -1 },
                new AgeItems() { name = "~ 25세", value = 0 },
                new AgeItems() { name = "26세 ~ 39세", value = 1 },
                new AgeItems() { name = "40세 이상", value = 2 },
            };
            ageValue.ItemsSource = age;
            ageValue.SelectedValue = -1;

            gender = new List<GenderItems>()
            {
                new GenderItems() { name = "< 선택 >", value = -1 },
                new GenderItems() { name = "Male", value = 0 },
                new GenderItems() { name = "Female", value = 1 },
            };
            genderValue.ItemsSource = gender;
            genderValue.SelectedValue = -1;

            caffeine = new List<CaffeineItems>()
            {
                new CaffeineItems() { name = "< 선택 >", value = -1 },
                new CaffeineItems() { name = "Yes", value = 0 },
                new CaffeineItems() { name = "No", value = 1 },
            };
            caffeineValue.ItemsSource = caffeine;
            caffeineValue.SelectedValue = -1;

            milk = new List<MilkItems>()
            {
                new MilkItems() { name = "< 선택 >", value = -1 },
                new MilkItems() { name = "Yes", value = 0 },
                new MilkItems() { name = "No", value = 1 },
            };
            milkValue.ItemsSource = milk;
            milkValue.SelectedValue = -1;

            sweetness = new List<SweetnessItems>()
            {
                new SweetnessItems() { name = "< 선택 >", value = -1 },
                new SweetnessItems() { name = "Low", value = 0 },  
                new SweetnessItems() { name = "Normal", value = 1 },
                new SweetnessItems() { name = "High", value = 2 },

            };
            sweetnessValue.ItemsSource = sweetness;
            sweetnessValue.SelectedValue = -1;

            price = new List<PriceItems>()
            {
                new PriceItems() { name = "< 선택 >", value = -1 },
                new PriceItems() { name = "Low", value = 0 },
                new PriceItems() { name = "Normal", value = 1 },
                new PriceItems() { name = "High", value = 2 },
            };
            priceValue.ItemsSource = price;
            priceValue.SelectedValue = -1;

            drink = new List<DrinkItems>()
            {
                new DrinkItems() { name = "< 선택 >", value = -1 },
                new DrinkItems() { name = "Fruit Smoothie", value = 0 },
                new DrinkItems() { name = "Americano", value = 1 },
                new DrinkItems() { name = "Citron Tea", value = 2 },
                new DrinkItems() { name = "Chocolate", value = 3 },
                new DrinkItems() { name = "Caffe Latte", value = 4 },
                new DrinkItems() { name = "Caffe Mocha", value = 5 },
                new DrinkItems() { name = "HerbTea", value = 6 },    
            };
            drinkValue.ItemsSource = drink;
            drinkValue.SelectedValue = -1;
        }

        public class AgeItems
        {
            public string name { get; set; }
            public int value { get; set; }
        }
        public class GenderItems
        {
            public string name { get; set; }
            public int value { get; set; }
        }
        public class CaffeineItems
        {
            public string name { get; set; }
            public int value { get; set; }
        }
        public class MilkItems
        {
            public string name { get; set; }
            public int value { get; set; }
        }
        public class SweetnessItems
        {
            public string name { get; set; }
            public int value { get; set; }
        }
        public class PriceItems
        {
            public string name { get; set; }
            public int value { get; set; }
        }
        public class DrinkItems
        {
            public string name { get; set; }
            public int value { get; set; }
        }

        private void oneR_Checked(object sender, RoutedEventArgs e)
        {
            labelDrink.Visibility = Visibility.Hidden;
            drinkValue.Visibility = Visibility.Hidden;

            labelOutput.Visibility = Visibility.Visible;
            labelClass.Visibility = Visibility.Visible;
            classValue.Visibility = Visibility.Visible;
            labelAccuracy.Visibility = Visibility.Visible;
            accuracy.Visibility = Visibility.Visible;
            labelResult.Visibility = Visibility.Visible;
            scrollResult.Visibility = Visibility.Visible;
            labelAssocResult.Visibility = Visibility.Hidden;
            scrollAssocResult.Visibility = Visibility.Hidden;
        }

        private void bayesian_Checked(object sender, RoutedEventArgs e)
        {
            labelDrink.Visibility = Visibility.Hidden;
            drinkValue.Visibility = Visibility.Hidden;

            labelOutput.Visibility = Visibility.Visible;
            labelClass.Visibility = Visibility.Visible;
            classValue.Visibility = Visibility.Visible;
            labelAccuracy.Visibility = Visibility.Visible;
            accuracy.Visibility = Visibility.Visible;
            labelResult.Visibility = Visibility.Visible;
            scrollResult.Visibility = Visibility.Visible;
            labelAssocResult.Visibility = Visibility.Hidden;
            scrollAssocResult.Visibility = Visibility.Hidden;
        }

        private void tree_Checked(object sender, RoutedEventArgs e)
        {
            labelDrink.Visibility = Visibility.Hidden;
            drinkValue.Visibility = Visibility.Hidden;

            labelOutput.Visibility = Visibility.Visible;
            labelClass.Visibility = Visibility.Visible;
            classValue.Visibility = Visibility.Visible;
            labelAccuracy.Visibility = Visibility.Visible;
            accuracy.Visibility = Visibility.Visible;
            labelResult.Visibility = Visibility.Visible;
            scrollResult.Visibility = Visibility.Visible;
            labelAssocResult.Visibility = Visibility.Hidden;
            scrollAssocResult.Visibility = Visibility.Hidden;
        }

        private void covering_Checked(object sender, RoutedEventArgs e)
        {
            labelDrink.Visibility = Visibility.Hidden;
            drinkValue.Visibility = Visibility.Hidden;

            labelOutput.Visibility = Visibility.Visible;
            labelClass.Visibility = Visibility.Visible;
            classValue.Visibility = Visibility.Visible;
            labelAccuracy.Visibility = Visibility.Visible;
            accuracy.Visibility = Visibility.Visible;
            labelResult.Visibility = Visibility.Visible;
            scrollResult.Visibility = Visibility.Visible;
            labelAssocResult.Visibility = Visibility.Hidden;
            scrollAssocResult.Visibility = Visibility.Hidden;
        }

        private void association_Checked(object sender, RoutedEventArgs e)
        {
            labelDrink.Visibility = Visibility.Visible;
            drinkValue.Visibility = Visibility.Visible;

            labelOutput.Visibility = Visibility.Hidden;
            labelClass.Visibility = Visibility.Hidden;
            classValue.Visibility = Visibility.Hidden;
            labelAccuracy.Visibility = Visibility.Hidden;
            accuracy.Visibility = Visibility.Hidden;
            labelResult.Visibility = Visibility.Hidden;
            scrollResult.Visibility = Visibility.Hidden;
            labelAssocResult.Visibility = Visibility.Visible;
            scrollAssocResult.Visibility = Visibility.Visible;
        }

        private void apply_Click(object sender, RoutedEventArgs e)
        {
            instances120.setClassIndex(instances120.numAttributes() - 1);
            instances140.setClassIndex(instances140.numAttributes() - 1);
            instances200.setClassIndex(instances200.numAttributes() - 1);

            if (oneR.IsChecked == true)
            {
                classifier = new weka.classifiers.rules.OneR();
                accuracy.Text = "35.8333%";
                print_Class(instances120);
            }
            else if (bayesian.IsChecked == true)
            {
                classifier = new weka.classifiers.bayes.NaiveBayes();
                accuracy.Text = "50%";
                print_Class(instances140);
            }
            else if (tree.IsChecked == true)
            {
                classifier = new weka.classifiers.trees.J48();
                accuracy.Text = "51.6667%";
                print_Class(instances120);
            }
            else if (covering.IsChecked == true)
            {
                classifier = new weka.classifiers.rules.Prism();
                accuracy.Text = "39.2857%";
                print_Class(instances140);

            }
            else if (association.IsChecked == true)
            {
                weka.associations.Apriori apriori = new weka.associations.Apriori();
                apriori.buildAssociations(instances200);
                assocResult.Text = apriori.toString();
            }
        }

        private void print_Class(Instances instances)
        {
            classifier.buildClassifier(instances);

            Instance test = new Instance(instances.numAttributes());
            test.setDataset(instances);

            if ((int)priceValue.SelectedValue != -1)
                test.setValue(0, (int)priceValue.SelectedValue);
            if ((int)sweetnessValue.SelectedValue != -1)
                test.setValue(1, (int)sweetnessValue.SelectedValue);
            if ((int)ageValue.SelectedValue != -1)
                test.setValue(2, (int)ageValue.SelectedValue);
            if ((int)genderValue.SelectedValue != -1)
                test.setValue(3, (int)genderValue.SelectedValue);
            if ((int)caffeineValue.SelectedValue != -1)
                test.setValue(4, (int)caffeineValue.SelectedValue);
            if ((int)milkValue.SelectedValue != -1)
                test.setValue(5, (int)milkValue.SelectedValue);

            if (classifier.classifyInstance(test) == 0)
                classValue.Text = "fruit_smoothie";
            else if (classifier.classifyInstance(test) == 1)
                classValue.Text = "americano";
            else if (classifier.classifyInstance(test) == 2)
                classValue.Text = "citron_tea";
            else if (classifier.classifyInstance(test) == 3)
                classValue.Text = "chocolate";
            else if (classifier.classifyInstance(test) == 4)
                classValue.Text = "caffe_latte";
            else if (classifier.classifyInstance(test) == 5)
                classValue.Text = "caffe_mocha";
            else if (classifier.classifyInstance(test) == 6)
                classValue.Text = "herb_tea";
            else
                classValue.Text = "default";

            result.Text = classifier.toString();
        }
    }
}
